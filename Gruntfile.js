module.exports = function(grunt){

    grunt.loadNpmTasks("grunt-contrib-connect");

    grunt.initConfig({

        connect: {
            server: {
                options: {
                    port: 8080,
                    base: 'app',
                    keepalive:true,
                    open:true
                }
            }
        }
    });

    // Register a task
    grunt.registerTask('run', ['connect']);
}